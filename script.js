const buttons = document.querySelectorAll(".btn")
// console.log(buttons);
document.addEventListener("keyup", (e) => {
    buttons.forEach(button => {
        button.style.color = "white"
        if (e.key === button.textContent || e.key === button.textContent.toLowerCase()){
            button.style.color = "blue"
        }
    })
})
let clickBtn;
document.addEventListener('click', (event) => {
  if(event.target !== event.currentTarget && event.target.classList.contains('btn')) {
    if(clickBtn) clickBtn.classList.remove('blue');
    event.target.classList.add('blue');
    clickBtn = event.target;
}
})